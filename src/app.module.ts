import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from '@nestjs/config';
import * as Joi from '@hapi/joi';
import { DatabaseModule } from './database/mongo/database.module'
import { FilesModule } from './files/files.module'
import { AuthModule } from './auth/auth.module';


@Module({
  imports: [
    ConfigModule.forRoot({
      validationSchema: Joi.object({
        AWS_ACCESS_KEY_ID: Joi.string().required(),
        AWS_SECRET_ACCESS_KEY: Joi.string().required(),
        AWS_PUBLIC_BUCKET_NAME: Joi.string().required(),
        WHITE_LIST: Joi.string().required(),
        SERVER_SWAGGER: Joi.string().required(),
        MONGO_CONNECTION_URI: Joi.string().required(),
        MONGO_TEST_CONNECTION_URI: Joi.string().required(),
        MAX_COUNT_FILE_MULTI: Joi.number().required(),
        MY_ACCESS_KEY_IMAGE_REMOTE: Joi.string().required(),
        KEY_JWT: Joi.string().required(),
      })
    }),
    FilesModule,
    DatabaseModule,
    AuthModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
