import {
  Body,
  Controller,
  HttpStatus,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBody, ApiResponse, ApiTags } from '@nestjs/swagger';
import { LoginDto } from 'src/users/dto/login.dto';
import { RegisterUserDto } from 'src/users/dto/register-user.dto';
import { User } from "src/users/schemas/user.schema";
import { AuthService } from './auth.service';

@ApiTags('auth')
@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        name: {
          type: 'string',
          example: 'Pedro Perez'
        },
        email: {
          type: 'string',
          example: 'pedroperez@gmail.com'
        },
        password: {
          type: 'string',
          example: 'mycontraseña',
        },
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'created files',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'not created files',
  })
  @Post('/register')
  register(@Body() registerUserDto: RegisterUserDto): Promise<User> {
    return this.authService.registerUser(registerUserDto);
  }

  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        email: {
          type: 'string',
          example: 'pedroperez@gmail.com'
        },
        password: {
          type: 'string',
          example: 'mycontraseña',
        },
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Iniciar Sesion',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'not created files',
  })
  @Post('/login')
  login(@Body() loginDto: LoginDto): Promise<{ accessToken: string }> {
    return this.authService.login(loginDto);
  }

  // @Patch('/change-password')
  // @UseGuards(AuthGuard())
  // changePassword(
  //   @Body() changePasswordDto: ChangePasswordDto,
  //   @GetUser() user: User,
  // ): Promise<void> {
  //   return this.authService.changePassword(changePasswordDto, user);
  // }
}
