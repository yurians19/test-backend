import {
  Injectable, UnauthorizedException,
} from '@nestjs/common';
import { LoginDto } from 'src/users/dto/login.dto';
import { RegisterUserDto } from 'src/users/dto/register-user.dto';
import { User } from "src/users/schemas/user.schema";
import { EncoderService } from './encoder.service';
import { JwtService } from '@nestjs/jwt';
import { JwtPayload } from './jwt-payload.interface';
import { UsersRepository } from 'src/users/users.repository';

@Injectable()
export class AuthService {
  constructor(
    private encoderService: EncoderService,
    private usersRepository: UsersRepository,
    private jwtService: JwtService,
  ) {}

  async registerUser(registerUserDto: RegisterUserDto): Promise<User> {
    const { name, email, password } = registerUserDto;
    const hashedPassword = await this.encoderService.encodePassword(password);
    console.log({ name, email, password: hashedPassword });
    return this.usersRepository.create({ name, email, password: hashedPassword });
  }

  async login(loginDto: LoginDto): Promise<{ accessToken: string }> {
    const { email, password } = loginDto;
    const user: User = await this.usersRepository.findOne({email});

    if (await this.encoderService.checkPassword(password, user.password)) {
        const payload: JwtPayload = { email };
        const accessToken = await this.jwtService.sign(payload);

        return { accessToken };
    }
    throw new UnauthorizedException('Please check your credentials');
  }
}
