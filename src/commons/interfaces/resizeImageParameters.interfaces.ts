export interface ResizeImageParameters {
    image: string
    width?: number
    height?: number
}