import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { createApi } from 'unsplash-js';
import nodeFetch from 'node-fetch';
import { HttpService } from '@nestjs/axios'
import * as url from 'url';
import * as sharp from 'sharp'
import { httpExceptionInvalidAttribute, httpExceptionNotFound } from '../types';
import { map } from 'rxjs';

@Injectable()
export class ImageRemoteService {
  private readonly logger = new Logger(ImageRemoteService.name)

  constructor(
    private readonly configService: ConfigService,
    private readonly httpService: HttpService
    ) {}

  async searchImageRemote(query: string): Promise<any> {
    try {
      const unsplash = await createApi({ accessKey: this.configService.get('MY_ACCESS_KEY_IMAGE_REMOTE'),fetch: nodeFetch });
      const result = await unsplash.search.getPhotos({
        query,
        page: 1,
        perPage: 10,
      });
      return result.response
      
    } catch (error) {
      throw new HttpException(httpExceptionInvalidAttribute, HttpStatus.UNPROCESSABLE_ENTITY)
    }
  }

  async getImageByUrl(urlImage: string): Promise<any> {
    try {
      const buffer = await this.httpService
        .get(urlImage,
        {
          responseType: 'arraybuffer'
        })
        .pipe(map((response) => response))
        .toPromise()

      const url_parts = url.parse(urlImage);
      const metadata = await sharp(buffer.data).metadata()
      return {
        originalname: `${url_parts.pathname.substring(1)}.${metadata.format}`,
        mimetype: buffer.headers['content-type'],
        size: metadata.size,
        buffer: buffer.data
      }
    } catch (error) {
      throw new HttpException(httpExceptionNotFound, HttpStatus.NOT_FOUND);
    }
  }
}
