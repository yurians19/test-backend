import { Injectable, Logger } from '@nestjs/common'
import * as sharp from 'sharp'

@Injectable()
export class SharpService {
  private readonly logger = new Logger(SharpService.name)

  constructor() {}

  async imageResize(buffer,width,height,fit?) {
    const imageResize = await sharp(buffer)
    .resize(width, height, {
        fit: fit || 'contain'
      })
    .toBuffer()
    return imageResize
  }
}
