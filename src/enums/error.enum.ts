export enum EError {
  file_type_not_match = 'File type not allowed!',
  params_not_valid = 'Validation failed',
  file_not_found = 'Not found file',
}
