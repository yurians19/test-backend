import { applyDecorators, HttpStatus } from '@nestjs/common'
import { ApiBody, ApiConsumes, ApiQuery, ApiResponse } from '@nestjs/swagger'
import { FileDocument } from '../schemas'

export function ApiUploadFile() {
  return applyDecorators(
    ApiConsumes('multipart/form-data'),
    ApiQuery({
      name: 'directory',
      type: 'string',
      description: 'Nombre de la carpeta de la entidad . Ej. Crops',
      example: 'crop'
    }),
    ApiBody({
      schema: {
        type: 'object',
        properties: {
          file: {
            type: 'string',
            format: 'binary',
          },
        },
      },
    }),
    ApiResponse({
      status: HttpStatus.CREATED,
      description: 'created file',
      type: FileDocument,
    }),
    ApiResponse({
      status: HttpStatus.BAD_REQUEST,
      description: 'not created file',
    })
  )
}
