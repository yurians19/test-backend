export enum EStatusFile {
  PENDING = 'PENDING',
  ACTIVE = 'ACTIVE',
  DISABLE = 'DISABLE',
  DELETED = 'DELETED'
}
