import { FilesService } from './files.service';
import {
  Controller,
  Get,
  Post,
  Res,
  UploadedFile,
  UseInterceptors,
  HttpStatus,
  Delete,
  Query,
  Body,
  UploadedFiles,
  HttpException,
  Req,
  Patch,
  UseGuards,
} from '@nestjs/common';
import {
  FileFieldsInterceptor,
  FileInterceptor,
} from '@nestjs/platform-express';
import { fileFilter } from '../utils';
import {
  ApiOkResponse,
  ApiResponse,
  ApiConsumes,
  ApiTags,
  ApiBody,
  ApiQuery,
  ApiNotFoundResponse,
  ApiBearerAuth
} from '@nestjs/swagger';
import { joiSchemaFile, joiSchema } from '../middlewares';
import { JoiValidationPipe } from '../pipes';
import { FileDocument } from './schemas';
import { EError } from '../enums';
import {
  apiNotFoundResponse,
  getResponseInvalid,
} from 'src/swagger/definitions';
import { httpExceptionNotFound } from 'src/commons';
import { EStatusFile } from './enum';
import { ApiUploadFile } from './decorators';
import { ImageRemoteService } from 'src/commons/services/image-remote.service';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';

@ApiTags('files')
@UseGuards(JwtAuthGuard)
@ApiBearerAuth()
@Controller({
  path: 'files',
  version: '1',
})
export class FilesController {
  constructor(
    private readonly filesService: FilesService,
    private readonly imageRemoteService: ImageRemoteService,
    ) {}

  @Post('private')
  @ApiConsumes('multipart/form-data')
  @ApiQuery({
    name: 'directory',
    type: 'string',
    description: 'Nombre de la carpeta donde guardara el archivo',
    example: 'Fotos'
  })
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        file: {
          type: 'string',
          format: 'binary',
        },
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'created file',
    type: FileDocument,
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'not created file',
  })
  @UseInterceptors(
    FileInterceptor('file', {
      fileFilter: fileFilter,
    }),
  )
  async uploadFilePrivate(
    @Query(new JoiValidationPipe(joiSchema)) query,
    @Body('file', new JoiValidationPipe(joiSchemaFile)) body,
    @UploadedFile('file') file: Express.Multer.File,
    @Res() res,
  ): Promise<FileDocument> {
    if (!file) {
      return res
        .status(HttpStatus.BAD_REQUEST)
        .json({ message: EError.file_not_found });
    }
    const data = await this.filesService.uploadFilePrivate(file, query);
    return res.status(HttpStatus.CREATED).json(data);
  }

  @Post()
  @ApiUploadFile()
  @UseInterceptors(
    FileInterceptor('file', {
      fileFilter,
    }),
  )
  async uploadFile(
    @Query(new JoiValidationPipe(joiSchema)) query,
    @Body('file', new JoiValidationPipe(joiSchemaFile)) body,
    @UploadedFile('file') file: Express.Multer.File,
    @Res() res,
  ) {
    if (!file) {
      return res
        .status(HttpStatus.BAD_REQUEST)
        .json({ message: EError.file_not_found });
    }
    const data = await this.filesService.uploadFile(file, query);
    return res.status(HttpStatus.CREATED).json(data);
  }

  @Post('private/multi')
  @ApiConsumes('multipart/form-data')
  @ApiQuery({
    name: 'directory',
    type: 'string',
    description: 'Nombre de la carpeta de la entidad . Ej. Crops',
  })
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        file: {
          type: 'string',
          format: 'binary',
        },
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'created files',
    type: FileDocument,
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'not created files',
  })
  @UseInterceptors(
    FileFieldsInterceptor([{ name: 'files', maxCount: 10 }]),
    FileInterceptor('files', {
      fileFilter,
    }),
  )
  async uploadFilesPrivate(
    @Query(new JoiValidationPipe(joiSchema)) query,
    @Body('files', new JoiValidationPipe(joiSchemaFile)) body,
    @UploadedFiles() filesEntry: { files: Array<Express.Multer.File> },
    @Res() res,
  ) {
    const { files } = filesEntry || {};
    if (!files.length) {
      throw new HttpException(
        httpExceptionNotFound,
        HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
    const data = await this.filesService.uploadFilesPrivate(files, query);
    return res.status(HttpStatus.CREATED).json(data);
  }

  @Get()
  @ApiQuery({ name: 'fileKey', type: 'string', required: true })
  @ApiOkResponse({ description: 'Download file' })
  @ApiResponse(getResponseInvalid)
  @ApiNotFoundResponse(apiNotFoundResponse)
  async seeUploadedFile(@Query('fileKey') fileKey, @Req() req, @Res() res) {
    const status = Object.values(EStatusFile).filter(
      (item) => item !== EStatusFile.ACTIVE,
    );
    const fileDocument: FileDocument = await this.filesService.getFileDocument({
      status: { $nin: status },
      $or:[{ key: fileKey},{keyIntermediate: fileKey},{keyThumbnails: fileKey}]
    });

    if (!fileDocument) {
      throw new HttpException(httpExceptionNotFound, HttpStatus.NOT_FOUND);
    }
    const EXPIRES = 259200; // 3 days
    return res.redirect(
      this.filesService.getSignedUrl(fileKey, 'GET', EXPIRES),
    );
  }

  @Post('multi')
  @ApiConsumes('multipart/form-data')
  @ApiQuery({
    name: 'directory',
    type: 'string',
    description: 'Nombre de la carpeta de la entidad . Ej. Crops',
  })
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        file: {
          type: 'string',
          format: 'binary',
        },
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'created files',
    type: FileDocument,
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'not created files',
  })
  @UseInterceptors(
    FileFieldsInterceptor([{ name: 'files', maxCount: 10 }]),
    FileInterceptor('files', {
      fileFilter,
    }),
  )
  async uploadFiles(
    @Query(new JoiValidationPipe(joiSchema)) query,
    @Body('files', new JoiValidationPipe(joiSchemaFile)) body,
    @UploadedFiles() filesEntry: { files: Array<Express.Multer.File> },
    @Res() res,
  ) {
    const { files } = filesEntry || {};
    if (!files.length) {
      throw new HttpException(
        httpExceptionNotFound,
        HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
    const data = await this.filesService.uploadFiles(files, query);
    return res.status(HttpStatus.CREATED).json(data);
  }

  @Delete()
  @ApiQuery({ name: 'fileKey', type: 'string' })
  @ApiOkResponse({ description: 'Deleted file' })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: EError.file_not_found,
  })
  async deleteFile(@Query('fileKey') fileKey, @Res() res) {
    const fileDocument = await this.filesService.updateFileDocument(
      { key: fileKey },
      { status: EStatusFile.DELETED },
    );
    if (!fileDocument) {
      throw new HttpException(httpExceptionNotFound, HttpStatus.NOT_FOUND);
    }
    return res.status(HttpStatus.OK).json(fileDocument);
  }
  
  @UseGuards(JwtAuthGuard)
  @Patch('update-name')
  @ApiQuery({ name: 'fileKey', type: 'string' })
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        newNameFile: {
          type: 'string',
          example: 'Nuevo Nombre'
        },
      },
    },
  })
  @ApiOkResponse({ description: 'Update name file' })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: EError.file_not_found,
  })
  async updateNameFile(@Query('fileKey') fileKey, @Body('newNameFile') newNameFile, @Res() res) {
    const fileDocument = await this.filesService.updateFileDocument(
      { key: fileKey },
      { nameFile : newNameFile },
    );
    if (!fileDocument) {
      throw new HttpException(httpExceptionNotFound, HttpStatus.NOT_FOUND);
    }
    return res.status(HttpStatus.OK).json(fileDocument);
  }

  @Get('search-image-remote')
  @ApiQuery({ name: 'query', type: 'string' })
  @ApiOkResponse({ description: 'Search Image Online' })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: EError.file_not_found,
  })
  async searchImageOnline(@Query('query') query, @Res() res) {
    const result = await this.imageRemoteService.searchImageRemote(query);
    return res.status(HttpStatus.OK).json(result);
  }

  @Post('upload-image-remote')
  @ApiQuery({ name: 'urlImage', type: 'string' })
  @ApiOkResponse({ description: 'Upload Image Remote' })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: EError.file_not_found,
  })
  async uploadImageRemote(@Query('urlImage') query, @Res() res) {
    const result = await this.filesService.uploadImageRemote(query);
    return res.status(HttpStatus.OK).json(result);
  }
}
