import { HttpModule } from '@nestjs/axios'
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ConfigService } from '@nestjs/config';
import { FileDocument, FileDocumentSchema } from './schemas/files.schema';
import { FilesController } from './files.controller';
import { FilesRepository } from './files.repository';
import { FilesService } from './files.service';
import { SharpService } from 'src/commons';
import { ImageRemoteService } from 'src/commons/services/image-remote.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: FileDocument.name, schema: FileDocumentSchema },
    ]),
    HttpModule
  ],
  controllers: [FilesController],
  providers: [FilesService, FilesRepository, ConfigService, SharpService, ImageRemoteService],
})
export class FilesModule {}
