import { Injectable } from '@nestjs/common'
import { InjectModel } from '@nestjs/mongoose'
import { Model } from 'mongoose'
import { EntityRepository } from '../database/mongo/entity.repository'
import { FilesDocument, FileDocument } from './schemas'

@Injectable()
export class FilesRepository extends EntityRepository<FilesDocument> {
  constructor(@InjectModel(FileDocument.name) fileModel: Model<FilesDocument>) {
    super(fileModel)
  }
}
