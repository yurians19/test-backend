import { Injectable, Logger } from '@nestjs/common';
import * as md5 from 'crypto-js/md5';
import * as sha256 from 'crypto-js/sha256';
import { S3 } from 'aws-sdk';
import * as stream from 'stream';
import { v4 as uuidV4 } from 'uuid';
import { ConfigService } from '@nestjs/config';
import { MimeContentEncoding, MimeTypes, MimeTypesName } from '../interfaces';
import { FileDocument } from './schemas/files.schema';
import { FilesRepository } from './files.repository';
import { IFilesDocument } from './interfaces';
import {
  amzCreateHmac,
  awsGetSignatureKey,
  fileParse,
  getAmzDate,
  ignoreResize,
} from '../utils';
import { SharpService } from 'src/commons';
import { FilterQuery, UpdateQuery } from 'mongoose';
import { ImageRemoteService } from 'src/commons/services/image-remote.service';

@Injectable()
export class FilesService {
  constructor(
    private readonly configService: ConfigService,
    private readonly filesRepository: FilesRepository,
    private readonly sharpService: SharpService,
    private readonly imageRemoteService: ImageRemoteService,
  ) {}

  async uploadFile(file, query): Promise<FileDocument> {
    return this.upload(file, query, 'public-read');
  }

  async uploadImageRemote(query: string): Promise<FileDocument> {
    console.log(query);
    
    const file = await this.imageRemoteService.getImageByUrl(query)

    return this.upload(file, query, 'public-read');
  }

  async uploadFilePrivate(file, query): Promise<FileDocument> {
    return this.upload(file, query);
  }

  async uploadFilesPrivate(files, query): Promise<FileDocument[]> {
    return Promise.all(
      files.map(async (file) => {
        return this.upload(file, query);
      }),
    );
  }

  async uploadFiles(files, query): Promise<FileDocument[]> {
    return Promise.all(
      files.map(async (file) => {
        return this.upload(file, query, 'public-read');
      }),
    );
  }

  // function deprecated
  async getFile(fileKey): Promise<any> {
    const s3 = new S3();
    let data = await s3
      .getObject({
        Bucket: this.configService.get('AWS_PUBLIC_BUCKET_NAME'),
        Key: fileKey,
      })
      .promise();
    return {
      body: data.Body,
      ContentType: data.ContentType,
    };
  }

  getSignedUrl(key, method, exp?): string {
    // our variables
    const access_key = this.configService.get('AWS_ACCESS_KEY_ID');
    const secret_key = this.configService.get('AWS_SECRET_ACCESS_KEY');
    const region = this.configService.get('AWS_REGION');
    const url = `${this.configService.get(
      'AWS_PUBLIC_BUCKET_NAME',
    )}.s3.amazonaws.com`;
    const service = 's3';
    const pathKey = `/${key}`;
    //X-Amz-Expires must be less than a week (in seconds); that is, the given X-Amz-Expires must be less than 604800 seconds
    const expires = exp || 3600; //1 hour
    // get the various date formats needed to form our request
    const amzDate = getAmzDate(new Date().toISOString());
    const authDate = amzDate.split('T')[0];

    // create our canonical request
    const canonicalReq = `${method}\n${pathKey}\nX-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=${access_key}%2F${authDate}%2F${region}%2F${service}%2Faws4_request&X-Amz-Date=${amzDate}&X-Amz-Expires=${expires}&X-Amz-SignedHeaders=host\nhost:${url}\n\nhost\nUNSIGNED-PAYLOAD`;

    const canonicalReqHash = sha256(canonicalReq).toString();

    // form our String-to-Sign
    const stringToSign = `AWS4-HMAC-SHA256\n${amzDate}\n${authDate}/${region}/${service}/aws4_request\n${canonicalReqHash}`;

    const signingKey = awsGetSignatureKey(
      secret_key,
      authDate,
      region,
      service,
    );

    // Sign our String-to-Sign with our Signing Key
    const signing = amzCreateHmac(signingKey, stringToSign, 'hex');

    return `https://${url}${pathKey}?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=${access_key}%2F${authDate}%2F${region}%2F${service}%2Faws4_request&X-Amz-Date=${amzDate}&X-Amz-Expires=${expires}&X-Amz-SignedHeaders=host&X-Amz-Signature=${signing}`;
  }

  getFileReadStream(fileKey, res, handleError): stream.Readable | any {
    const s3 = new S3();
    s3.getObject({
      Bucket: this.configService.get('AWS_PUBLIC_BUCKET_NAME'),
      Key: fileKey,
    })
      .createReadStream()
      .on('error', handleError)
      .pipe(res)
      .on('end', res.end);
  }
  // function deprecated
  async deleteFile(fileKey): Promise<any> {
    const s3 = new S3();
    return s3
      .deleteObject({
        Bucket: this.configService.get('AWS_PUBLIC_BUCKET_NAME'),
        Key: fileKey,
      })
      .promise();
  }

  private async upload(file: any, query, ACL?): Promise<FileDocument> {
    const { buffer } = file;
    const codeMd5 = md5(file.buffer.toString('hex')).toString();

    const data: S3.ManagedUpload.SendData = await this.uploadS3(
      file,
      buffer,
      ACL,
      query,
    );

    const isPrivate = ACL !== 'public-read';

    const filesDocument: IFilesDocument = {
      nameFile: file.originalname,
      mimetype: file.mimetype,
      codeMd5,
      size: file.size,
      encoding: MimeContentEncoding[file.mimetype],
      key: data.Key,
      path: isPrivate ? null : data?.Location ?? null,
      isPrivate,
    };

    const fileDocument = await this.createFileDocument(filesDocument);
    this.generateImagesResize(fileDocument, file, ACL, query).then();
    return fileDocument;
  }

  async uploadS3(file, buffer, ACL, query): Promise<S3.ManagedUpload.SendData> {
    const { directory = 'remote', entityId = uuidV4() } = query;
    const s3 = new S3();
    const params: S3.PutObjectRequest = {
      Bucket: this.configService.get('AWS_PUBLIC_BUCKET_NAME'),
      Body: buffer,
      ACL: ACL ?? undefined,
      ContentType: MimeTypes[file.mimetype],
      ContentEncoding: MimeContentEncoding[file.mimetype],
      Key: `${directory}/${entityId}/${
        MimeTypesName[file.mimetype]
      }/${fileParse(file.originalname)}`,
    };

    return s3.upload(params).promise();
  }

  async createFileDocument(
    filesDocument: IFilesDocument,
  ): Promise<FileDocument> {
    return this.filesRepository.create(filesDocument);
  }

  async updateFileDocument(
    filesDocument: FilterQuery<FileDocument>,
    updateEntityData: UpdateQuery<unknown>,
  ): Promise<FileDocument> {
    return this.filesRepository.findOneAndUpdate(
      filesDocument,
      updateEntityData,
    );
  }

  async getFileDocument(query): Promise<FileDocument> {
    return this.filesRepository.findOne(query);
  }

  async generateImagesResize(fileDocument, file, ACL, query) {
    try {
      if (
        MimeTypesName[file.mimetype] === 'images' &&
        !ignoreResize(file.mimetype)
      ) {
        const { buffer } = file;
        const bufferIntermediate = await this.sharpService.imageResize(
          buffer,
          500,
          500,
        );
        const bufferThumbnail = await this.sharpService.imageResize(
          buffer,
          200,
          200,
        );
        const dataIntermediate: S3.ManagedUpload.SendData = await this.uploadS3(
          file,
          bufferIntermediate,
          ACL,
          query,
        );
        const dataThumbnail = await this.uploadS3(
          file,
          bufferThumbnail,
          ACL,
          query,
        );
        fileDocument.keyIntermediate = dataIntermediate?.Key ?? null;
        fileDocument.keyThumbnails = dataThumbnail?.Key ?? null;
        fileDocument.pathIntermediate = fileDocument.isPrivate
          ? null
          : dataIntermediate?.Location ?? null;
        fileDocument.pathThumbnails = fileDocument.isPrivate
          ? null
          : dataThumbnail?.Location ?? null;
        await fileDocument.save();
      }
    } catch (err) {
      Logger.error(err.toString());
    }
  }
}
