import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { ApiProperty } from '@nestjs/swagger'
import { Document, Schema as mongooseSchema } from 'mongoose'
import { EStatusFile } from '../enum'
export type FilesDocument = FileDocument & Document

@Schema({
  timestamps: true,
})
export class FileDocument {
  @Prop()
  @ApiProperty()
  path: string

  @Prop()
  @ApiProperty()
  pathIntermediate: string

  @Prop()
  @ApiProperty()
  pathThumbnails: string

  @Prop()
  @ApiProperty()
  nameFile: string

  @Prop()
  @ApiProperty()
  date: string

  @Prop()
  @ApiProperty()
  key: string

  @Prop()
  @ApiProperty()
  keyIntermediate: string

  @Prop()
  @ApiProperty()
  keyThumbnails: string

  @Prop()
  @ApiProperty()
  mimetype: String

  @Prop()
  @ApiProperty()
  size: Number

  @Prop()
  @ApiProperty()
  encoding: String

  @Prop()
  @ApiProperty()
  codeMd5: String

  @Prop({ required: true, default: EStatusFile.ACTIVE })
  @ApiProperty({ required: true, default: EStatusFile.ACTIVE })
  status: String

  @Prop({default: false })
  isPrivate: boolean
}

export const FileDocumentSchema = SchemaFactory.createForClass(FileDocument)
