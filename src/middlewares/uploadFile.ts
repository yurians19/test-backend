const Joi = require('@hapi/joi');
Joi.objectId = require('joi-objectid')(Joi);

export const joiSchemaFile = Joi.object({
  file: Joi.any().required().meta({ swaggerType: 'file' }),
});

export const joiSchema = Joi.object({
  entityId: Joi.string(),
  directory: Joi.string().required(),
});

export const joiUpdateNameFileSchema = Joi.object({
  fileKey: Joi.string().required(),
  newNameFile: Joi.string().required(),
});
