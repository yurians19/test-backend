import { ApiBodyOptions } from '@nestjs/swagger'
import { AdminUpdate } from 'src/files/dto'

export const apiBodyAdminsUpdate: ApiBodyOptions = {
  description: 'The requested Body',
  type: AdminUpdate,
}
