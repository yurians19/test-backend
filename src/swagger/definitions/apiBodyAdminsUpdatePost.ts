import { ApiBodyOptions } from '@nestjs/swagger'
import { AdminInvitation } from 'src/files/dto'

export const apiBodyAdminsUpdatePost: ApiBodyOptions = {
  description: 'The requested Body',
  type: AdminInvitation,
}
