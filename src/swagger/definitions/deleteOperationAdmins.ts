import { ApiOperationOptions } from "@nestjs/swagger";

export const deleteOperationAdmins: Partial<ApiOperationOptions> = { 
  summary: 'Delete or disable an admin', 
  description: 'Delete or disable an admin' 
}
