import { ApiResponseOptions } from '@nestjs/swagger'
import { AdminDto } from 'src/files/dto'

export const getOkResponseAdminsUpdate: ApiResponseOptions = {
  description: 'Success',
  type: AdminDto,
}
