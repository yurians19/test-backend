import { ApiOperationOptions } from '@nestjs/swagger'

export const getOperationAdmins: Partial<ApiOperationOptions> = { 
  summary: 'Partial admin edit', 
  description: 'Partial admin edit' 
}
