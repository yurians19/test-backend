import { ApiBodyOptions } from '@nestjs/swagger'
import { AdminDto } from 'src/files/dto'

export const postCreatedResponse: ApiBodyOptions = {
  description: 'Success',
  type: AdminDto
}
