import { ApiOperationOptions } from '@nestjs/swagger'

export const postOperationAdmins: Partial<ApiOperationOptions> = { 
  summary: 'Invite a new admin', 
  description: 'Invite a new admin' 
}
