import * as crypto from "crypto";

// this function converts the generic JS ISO8601 date format to the specific format the AWS API wants
export const getAmzDate = (dateStr): string => {
    const chars = [":","-"];
    for (const i in chars) {
        while (dateStr.indexOf(chars[i]) != -1) {
            dateStr = dateStr.replace(chars[i],"");
        }
    }
    dateStr = dateStr.split(".")[0] + "Z";
    return dateStr;
}
export const amzCreateHmac = (key: string, string: string, encoding?: string | any): string => crypto.createHmac('sha256', key).update(string, 'utf8').digest(encoding)

export const awsGetSignatureKey = (key: string, dateStamp: string, regionName: string, serviceName: string): string => {

    const kDate = amzCreateHmac(`AWS4${key}`, dateStamp);

    const kRegion = amzCreateHmac(kDate, regionName);

    const kService = amzCreateHmac(kRegion, serviceName );

    const kSigning = amzCreateHmac(kService, 'aws4_request');

    return kSigning;
}

