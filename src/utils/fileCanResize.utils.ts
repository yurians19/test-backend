import { IGNORE_RESIZE_MYME_TYPES } from '../commons/constants';

export const ignoreResize = (mimeType: string): boolean => {
  return IGNORE_RESIZE_MYME_TYPES.includes(mimeType);
};
