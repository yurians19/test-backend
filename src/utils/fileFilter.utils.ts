import { BadRequestException } from '@nestjs/common';
import { MimeTypes } from '../interfaces';
import { EError } from '../enums';

export const fileFilter = (req, file, callback) => {
  if (!MimeTypes[file.mimetype]) {
    return callback(new BadRequestException(EError.file_type_not_match), false);
  }
  callback(null, true);
};
