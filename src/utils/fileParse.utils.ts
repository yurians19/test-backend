import { v4 as uuid } from 'uuid';
export const fileParse = (filename) => {
  const fileNameParts = filename.split('.')
  const fileExt = new String(fileNameParts[fileNameParts.length - 1]).toLocaleLowerCase().trim()
  return `file-${uuid()}-${Date.now()}.${fileExt}`
};
