import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication, HttpStatus } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';

describe('files (e2e)', () => {
  let app: INestApplication;
  let fileKey: String = '';

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [
        AppModule
    ],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('Create File Private successfully',async () => {
    return request(app.getHttpServer())
      .post('/files/private')
      .attach('file','test/files/pdf-test.pdf')
      .query({directory: 'license', entityId: '60da330691500058affaf70e' })
      .expect(HttpStatus.CREATED)
      .expect(({ body }) => {
        fileKey = body.key
      });
  },30000);

  it('get one file private',async () => {
    return request(app.getHttpServer())
        .get(`/files`)
        .query({fileKey})
        .expect(HttpStatus.OK)
  },30000);

  it('delete a file successfully',async () => {
     return request(app.getHttpServer())
        .delete(`/files`)
        .query({fileKey})
        .expect(HttpStatus.OK)
  },30000);

  it('get a file not exist',async () => {
    return request(app.getHttpServer())
        .get(`/files`)
        .query({fileKey})
        .expect(HttpStatus.BAD_REQUEST)
        .expect(({ body }) => {
            expect(body.message).toContain("key");
        });
  },30000);

  afterAll(async () => {
    await app.close();
  });
});
